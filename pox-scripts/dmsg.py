# Copyright (c) 2015 Mohamed Aslan <maslan@sce.carleton.ca>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

import zmq
import thread
import time
import json

"""
TODO:
-+-+-+

* add support for (N)
* add support for lazy pirate client: http://zguide.zeromq.org/py:lpclient

"""
REQUEST_TIMEOUT = 2000

class Connection:

	def __init__(self, callback):
		self.peers = dict()
		self.ctx = zmq.Context()
		self.server = self.ctx.socket(zmq.REP)
		self.server.bind("tcp://*:5000")
		self.callback = callback
		self.poll = zmq.Poller()
		print("[dmsg] connection initialized.")

	def setConsistency(self, N, R, W):
		self.N = N
		self.R = R
		self.W = W
		print("[dmsg] consistency level set to: Replicas(N) = %d, Reads(R) = %d, Writes(W) = %d." % (N, R, W))

	def addPeer(self, ip_addr):
		print("[dmsg] adding peer %s." % ip_addr)
		self.peers[ip_addr] = None;

	def start(self):
		thread.start_new_thread(self.server_thread, (None,))
		#time.sleep(1)
		for peer in self.peers:
			print("[dmsg] connecting to peer %s." % peer)
			self.peers[peer] = self.ctx.socket(zmq.REQ)
			self.peers[peer].connect("tcp://" + peer + ":5000")
			self.poll.register(self.peers[peer], zmq.POLLIN)
			print("[dmsg] connected.")

	def server_thread(self, args):
		while True:
			msg = self.server.recv()
			print("[dmsg] asked about: %s" % msg)
			resp = self.callback(msg)
			self.server.send(resp)

	def send_r(self, msg, retries):
		replies = dict()
		for peer in self.peers:
			print("[dmsg] sending message to peer %s." % peer)
			self.peers[peer].send(msg)
			print("[dmsg] waiting for reply.")
		socks = dict(self.poll.poll(REQUEST_TIMEOUT))
		for peer in self.peers:
			if self.peers[peer] in socks and socks[self.peers[peer]] == zmq.POLLIN:
				replies[peer] = self.peers[peer].recv()
			else:
				self.peers[peer].setsockopt(zmq.LINGER, 0)
				self.peers[peer].close()
				self.poll.unregister(self.peers[peer])
				# re-connect
				self.peers[peer] = self.ctx.socket(zmq.REQ)
				self.peers[peer].connect("tcp://" + peer + ":5000")
				self.poll.register(self.peers[peer], zmq.POLLIN)
				#self.peers[peer].send(msg)
				print("[dmsg] re-connected.")
				if retries == 0:
					return json.dumps(replies)
				print("[dmsg] re-trying.")
				self.send_r(msg, retries - 1)
		return json.dumps(replies)

	def send(self, msg):
		return self.send_r(msg, 1)

