# Copyright (c) 2015 Mohamed Aslan <maslan@sce.carleton.ca>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

from pox.core import core
import pox.openflow.libopenflow_01 as of
from pox.lib.addresses import IPAddr, EthAddr
from pox.lib.packet.ethernet import ethernet
from threading import Lock
import time

import random

IDLE_TIMEOUT = of.OFP_FLOW_PERMANENT
HARD_TIMEOUT = 2


class Host(object):
	def __init__(self, ip_addr, mac_addr = None):
		#self.ip_addr = IPAddr(ip_addr)
		self.ip_addr = ip_addr
		self.mac_addr = mac_addr

class Server(object):
	def __init__(self, ip_addr, mac_addr, port, gateway_switch):
		self.ip_addr = IPAddr(ip_addr)
		self.mac_addr = EthAddr(mac_addr)
		self.port = port
		self.gateway_switch = gateway_switch
		self.stats = 0
		self.utilization = 0
		#self.n_flows = random.randint(5, 10)
		self.n_flows = 0
		#self.flow_list = list()
		self.flow_list = set()
		self.start = time.time()
		self.path = "."
		self.packets = 0
		self.drops = 0
	def set_log_path(self, p):
		self.path = p
	def insert_flow(self, f):
		if f in self.flow_list:
			return
		#self.flow_list.append(f)
		self.flow_list.add(f)
		now = time.time() - self.start
		with open(self.path + "/events-" + self.ip_addr.toStr() + ".csv", "a") as f:
			#f.write("%d, %d, \'i\'\n" % (now, len(self.flow_list)))
			f.write("%.4f, %d, \'i\', %s\n" % (now, len(self.flow_list), self.flow_list))
	def remove_flow(self, f):
		if f not in self.flow_list:
			return
		self.flow_list.remove(f)
		now = time.time() - self.start
		with open(self.path + "/events-" + self.ip_addr.toStr() + ".csv", "a") as f:
			#f.write("%d, %d, \'r\'\n" % (now, len(self.flow_list)))
			f.write("%.4f, %d, \'r\', %s\n" % (now, len(self.flow_list), self.flow_list))
	def find_flow(self, f):
		if f in self.flow_list:
			return True
		else:
			return False
	def set_utilization(self, u):
		self.utilization = u
	def get_utilization(self):
		return self.utilization
	def update(self):
		self.n_flows = len(self.flow_list)
	def get_flow_count(self):
		self.update()
		return self.n_flows

class OpenFlow(object):
	@staticmethod
	def do_create_path(conn, h1, h2, out_port, timeout_idle, timeout_hard, feedback=False, bid=None):
		# h1 -> h2
		print "DEBUG: creating path from ", h1.ip_addr, " to ", h2.ip_addr, " at ", out_port
		msg = of.ofp_flow_mod()
		msg.priority = of.OFP_DEFAULT_PRIORITY
		msg.idle_timeout = timeout_idle
		msg.hard_timeout = timeout_hard
		msg.match.dl_type = ethernet.IP_TYPE
		msg.match.nw_src = h1.ip_addr
		msg.match.nw_dst = h2.ip_addr
		if bid is not None and bid != -1:
			msg.buffer_id = bid
		msg.actions.append(of.ofp_action_output(port = out_port))
		if feedback == True:
			msg.flags |= of.OFPFF_SEND_FLOW_REM
		conn.send(msg)
	@staticmethod
	def do_create_bi_forward_path(conn, h1, h2, h3, port_to_3, port_to_1, timeout_idle, timeout_hard, feedback=False, bid=None, data=None):
		# [h1 <-> h2] -> [h1 <-> h3 <-> h2]
		print "DEBUG: creating bi-forwarding path from ", h1.ip_addr, " to ", h2.ip_addr, " via ", h3.ip_addr
		msg = of.ofp_flow_mod()
		msg.priority = of.OFP_DEFAULT_PRIORITY
		msg.idle_timeout = timeout_idle
		msg.hard_timeout = timeout_hard
		msg.match.dl_type = ethernet.IP_TYPE
		msg.match.nw_src = h1.ip_addr
		msg.match.nw_dst = h2.ip_addr
		if bid is not None and bid != -1:
			msg.buffer_id = bid
		elif data is not None:
			msg.data = data
		msg.actions.append(of.ofp_action_nw_addr.set_dst(h3.ip_addr))
		msg.actions.append(of.ofp_action_dl_addr.set_dst(h3.mac_addr))
		msg.actions.append(of.ofp_action_output(port = port_to_3))
		if feedback == True:
			msg.flags |= of.OFPFF_SEND_FLOW_REM
		conn.send(msg)
		msg = of.ofp_flow_mod()
		msg.priority = of.OFP_DEFAULT_PRIORITY
		msg.idle_timeout = timeout_idle
		msg.hard_timeout = timeout_hard
		msg.match.dl_type = ethernet.IP_TYPE
		msg.match.nw_src = h3.ip_addr
		msg.match.nw_dst = h1.ip_addr
		msg.actions.append(of.ofp_action_nw_addr.set_src(h2.ip_addr))
		msg.actions.append(of.ofp_action_dl_addr.set_src(h2.mac_addr))
		msg.actions.append(of.ofp_action_output(port = port_to_1))
		msg.flags = 0
		conn.send(msg)
	@staticmethod
	def create_path(conn, h1, h2, out_port, feedback=False, bid=None):
		OpenFlow.do_create_path(conn, h1, h2, out_port, IDLE_TIMEOUT, HARD_TIMEOUT, feedback, bid)
	@staticmethod
	def create_path_4ever(conn, h1, h2, out_port):
		OpenFlow.do_create_path(conn, h1, h2, out_port, of.OFP_FLOW_PERMANENT, of.OFP_FLOW_PERMANENT)
	@staticmethod
	def create_bi_forward_path(conn, h1, h2, h3, port_to_3, port_to_1, feedback=False, bid=None, data=None):
		OpenFlow.do_create_bi_forward_path(conn, h1, h2, h3, port_to_3, port_to_1, IDLE_TIMEOUT, HARD_TIMEOUT, feedback, bid, data)
	@staticmethod
	def replay_packet_to(conn, pkt, port, h, buf_id = None, data = None):
		#"""
		msg = of.ofp_packet_out()
		msg.priority = of.OFP_DEFAULT_PRIORITY
		msg.match = of.ofp_match.from_packet(pkt)
		if buf_id is not None and buf_id != -1:
			msg.buffer_id = buf_id
		elif data is not None:
			msg.data = data
		msg.actions.append(of.ofp_action_nw_addr.set_dst(h.ip_addr))
		msg.actions.append(of.ofp_action_dl_addr.set_dst(h.mac_addr))
		msg.actions.append(of.ofp_action_output(port = port))
		conn.send(msg)
		#"""
		"""
		msg = of.ofp_packet_out(data = data)
		msg.actions.append(of.ofp_action_output(port = of.OFPP_IN_PORT))
		conn.send(msg)
		"""
	@staticmethod
	def replay_packet(conn, pkt, port, buf_id = None, data = None):
		"""
		msg = of.ofp_packet_out()
		msg.priority = of.OFP_DEFAULT_PRIORITY
		msg.match = of.ofp_match.from_packet(pkt)
		if buf_id is not None and buf_id != -1:
			msg.buffer_id = buf_id
		elif data is not None:
			msg.data = data
		msg.actions.append(of.ofp_action_output(port = port))
		"""
		#"""
		msg = of.ofp_packet_out(data = data)
		msg.actions.append(of.ofp_action_output(port = of.OFPP_IN_PORT))
		conn.send(msg)
		#"""
