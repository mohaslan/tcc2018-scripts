# Copyright (c) 2015 Mohamed Aslan <maslan@sce.carleton.ca>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

from __future__ import division
from pox.core import core
import pox.openflow.libopenflow_01 as of
from pox.lib.util import dpid_to_str
from pox.lib.util import str_to_bool
from pox.lib.packet.ipv4 import ipv4
from pox.lib.packet.arp import arp
import time
import thread
import json
import sys
import dmsg
from pox.lib.recoco import Timer
from pox.openflow.of_json import *
from threading import Lock

from virtserver import VirtualServer
from hosts import Server, Host, OpenFlow
from flowcache import FlowCache

import os.path

log = core.getLogger()

VIRT_SERVER_IP = IPAddr("10.0.0.254")
VIRT_SERVER_MAC = EthAddr("00:00:00:00:00:EF")

conn_list = dict()

conn = None

POLL_PERIOD = 2
CAPACITY = 100	# mbps

is_active = True
next_server = 0
thres = 0

tx_server = 0
rx_server = 0
last_server = 0
mutex = Lock()


PATH = "."	# log files path
DELTAU = 0.1

servers = [Server("10.0.0.3", "00:00:00:00:00:03", 3, '00-00-00-00-00-01'), Server("10.0.0.4", "00:00:00:00:00:04", 1, '00-00-00-00-00-02')]

class LoadBalancer(object):
	def __init__ (self, connection, myID):
		self.connection = connection

		self.myID = myID

		servers[0].set_log_path(PATH)

		self.tx_packets = 0
		self.rx_packets = 0
		self.tx_server = 0
		self.rx_server = 0
		self.tx_dropped = 0
		self.rx_dropped = 0
		self.last_server = 0

		self.all_tx_pkts = 0
		self.all_tx_bytes = 0
		self.diff_tx_pkts = 0
		self.diff_tx_bytes = 0
		self.all_rx_pkts = 0
		self.all_rx_bytes = 0
		self.diff_rx_pkts = 0
		self.diff_rx_bytes = 0

		self.macToPort = {}

		# virtual server
		self.vserver = VirtualServer(VIRT_SERVER_IP, VIRT_SERVER_MAC)

		connection.addListeners(self)

		self.can_log = False
		self.cache = FlowCache()
		Timer(POLL_PERIOD, self.poll_func, recurring=True)
		# sync clock with other modules
		thread.start_new_thread(self.clock, ())

	def clock(self):
		while not os.path.exists("/tmp/sync.lock"):
			time.sleep(0.5)
		self.can_log = True
		self.start_time = time.time()

	def next_server(self):
		global conn, servers, is_active, next_server, thres
		L0 = servers[0].get_utilization()
		L1 = servers[1].get_utilization()
		if L0 + L1 == 0:
			return servers[next_server]
		P0 = L0 / (12.5 * 1024 * 1024) #/ (L0 + L1)
		P1 = L1 / (12.5 * 1024 * 1024) #/ (L0 + L1)
		print "LSVS C1 is_active: ", str(is_active), ", S1: ", str(L0), ", S2: ", str(L1), ", L1: ", str(P0), ", L2: ", str(P1), ", Thres: ", str(thres)
		if is_active and P0 >= thres:
			print "LSVS C1: SYNC"
			resp = json.loads(conn.send(str(L0)))
			L1 = float(resp["10.0.0.2"])
			servers[1].set_utilization(L1)
			if L0 > L1:
				print "LSVS C1: is PC"
				is_active = False
				next_server = 1
		server = next_server
		print "server: ", server
		return servers[server]

	def poll_func(self):
		if self.can_log == True:
			now = time.time() - self.start_time
			with open(PATH + "/switch-port3-" + str(self.myID) + ".csv", "a") as f:
				f.write("%.2f, %d, %d, %d\n" % (now, servers[0].packets, servers[0].utilization, servers[0].drops))
			with open(PATH + "/switch-port-all-" + str(self.myID) + ".csv", "a") as ff:
				ff.write("%.2f, %d, %d, %d, %d\n" % (now, self.diff_tx_pkts, self.diff_tx_bytes, self.diff_rx_pkts, self.diff_rx_bytes))
		self.connection.send(of.ofp_stats_request(body=of.ofp_port_stats_request()))


	def _handle_PortStatsReceived(self, event):
		tot_rx = tot_rx_by = 0
		tot_tx = tot_tx_by = 0
		for p in event.stats:
			if 4 <= p.port_no <= 35:
				tot_rx += p.rx_packets
				tot_rx_by += p.rx_bytes
			elif p.port_no == 3:
				tot_tx += p.tx_packets
				tot_tx_by += p.tx_bytes

		self.diff_tx_pkts = tot_tx - self.all_tx_pkts
		self.diff_tx_bytes = tot_tx_by - self.all_tx_bytes
		self.diff_rx_pkts = tot_rx - self.all_rx_pkts
		self.diff_rx_bytes = tot_rx_by - self.all_rx_bytes

		self.all_tx_pkts = tot_tx
		self.all_tx_bytes = tot_tx_by
		self.all_rx_pkts = tot_rx
		self.all_rx_bytes = tot_rx_by


		for item in event.stats:
			if  item.port_no == 3:
				tx_pkts_diff = item.tx_packets - self.tx_packets
				rx_pkts_diff = item.rx_packets - self.rx_packets
				self.tx_packets = item.tx_packets
				self.rx_packets = item.rx_packets
				servers[0].packets = tx_pkts_diff

				tx_diff = item.tx_bytes - self.tx_server
				rx_diff = item.rx_bytes - self.rx_server
				self.tx_server = item.tx_bytes
				self.rx_server = item.rx_bytes
				load = tx_diff
				servers[0].set_utilization(load)

				tx_dropped_diff = item.tx_dropped - self.tx_dropped
				rx_dropped_diff = item.rx_dropped - self.rx_dropped
				self.tx_dropped = item.tx_dropped
				self.rx_dropped = item.rx_dropped
				servers[0].drops = tx_dropped_diff



	def _handle_FlowRemoved(self, event):
		#now = time.time() - self.start_time
		if event.hardTimeout:
			if event.ofp.match.dl_type == ethernet.IP_TYPE:
				#if event.ofp.match.nw_proto == ipv4.TCP_PROTOCOL or event.ofp.match.nw_proto == ipv4.UDP_PROTOCOL:
					servers[0].remove_flow((event.ofp.match.nw_src.toStr(), event.ofp.match.nw_dst.toStr()))
					#with open("removal1.csv", "a") as f:
					#	f.write("%d, %s, %s, %d %d\n" % (now, event.ofp.match.nw_src, event.ofp.match.nw_dst, servers[0].n_flows, servers[1].n_flows))

	def _handle_PacketIn(self, event):
		packet = event.parse()

		def drop():
			print "dropping"
			msg = of.ofp_flow_mod()
			msg.match = of.ofp_match.from_packet(packet)
			msg.idle_timeout = 10
			msg.hard_timeout = 30
			msg.buffer_id = event.ofp.buffer_id
			self.connection.send(msg)

		if packet.type == ethernet.IPV6_TYPE:
			return
			print "IPv6"
			# drop()

		elif packet.type == ethernet.IP_TYPE:
			ip_packet = packet.payload
			key = (ip_packet.srcip.toStr(), ip_packet.dstip.toStr())

			if self.cache.has(key) == True:
				to_ip = self.cache.getToIP(key)
				to_mac = self.cache.getToMAC(key)
				to_port = self.cache.getToPort(key)

				if ip_packet.dstip == servers[0].ip_addr:
					OpenFlow.replay_packet(self.connection, packet, servers[0].port, buf_id = event.ofp.buffer_id, data = event.ofp)
				else:
					OpenFlow.replay_packet_to(self.connection, packet, to_port, Host(to_ip, to_mac), buf_id = event.ofp.buffer_id, data = event.ofp)
				return

			if ip_packet.dstip == VIRT_SERVER_IP:
				server = self.next_server()

				if server.ip_addr == servers[0].ip_addr:
					print "LB ME"
					servers[0].insert_flow(key)
					OpenFlow.create_bi_forward_path(self.connection, Host(ip_packet.srcip), Host(self.vserver.ip_addr, self.vserver.mac_addr), Host(server.ip_addr, server.mac_addr), server.port, event.port, feedback=True, bid=event.ofp.buffer_id, data=event.ofp)
					self.cache.add(key, 1.5, server.ip_addr, server.mac_addr, server.port)
				else:
					print "LB OTHER"
					OpenFlow.create_path(self.connection, Host(ip_packet.srcip), Host(self.vserver.ip_addr), 1, bid=event.ofp.buffer_id)
					self.cache.add(key, 1.5, self.vserver.ip_addr, self.vserver.mac_addr, 1)


			else:
				print "XXX packet from: ", ip_packet.srcip, " to: ", ip_packet.dstip

		elif packet.type == ethernet.ARP_TYPE:
			#arp_packet = packet.payload
			arp_packet = packet.next
			print "ARP packet received"
			if arp_packet.opcode == arp.REQUEST:
				print "who has ", arp_packet.protodst, " tell ", arp_packet.protosrc
				if arp_packet.protodst == VIRT_SERVER_IP:
					self.vserver.send_arp_reply(self.connection, event.port, arp_packet)
				else:
					print "Else!"
					return

		else:
			print packet.src, " -> ", packet.dst, " : ", event.port, " -- ", self.connection
			print 'type: ', packet.type


class ControlApp(object):
	def __init__(self):
		global conn_list, conn
		core.openflow.addListeners(self)
		conn = dmsg.Connection(self.callback)
		conn.setConsistency(2, 1, 1)
		conn.addPeer("10.0.0.2")
		conn.start()


	def callback(self, msg):
		global servers, is_active, next_server, thres
		servers[1].set_utilization(float(msg))
		if servers[0].get_utilization() <= servers[1].get_utilization():
			print "LSVS C1: is AC"
			is_active = True
			next_server = 0
			# threshold: set as the load of the second least loaded server in the last synchronization plus delta_u
			p = servers[0].get_utilization() / (12.5 * 1024 * 1024)
			thres = min(1.0, p + DELTAU)
		else:
			print "LSVS INFO OUTDATED"
		#return "OK"
		return str(servers[0].get_utilization())

	def _handle_ConnectionDown(self, event):
		global conn_list
		dpid = dpid_to_str(event.connection.dpid)
		if dpid in conn_list:
			del conn_list[dpid]
		print "DOWN: ", dpid

	def _handle_ConnectionUp(self, event):
		global conn_list, conn
		print "DPID: ", dpid_to_str(event.connection.dpid)
		dpid = dpid_to_str(event.connection.dpid)
		if dpid in conn_list:
			print "Switch already connected!"
		conn_list[dpid] = event.connection
		if dpid == '00-00-00-00-00-01':
			print "Intra-domain Switch ... [connected]"

			thread.start_new_thread(self.bootstrap, (conn_list[dpid],))

			# start the load-balancer
			LoadBalancer(event.connection, 1)
		else:
			print "Inter-domain Switch ... [connected]"

	def bootstrap(self, connection):
		time.sleep(3)
		# connect controllers
		# h1 (c1) -> h2 (c2)
		OpenFlow.create_path_4ever(connection, Host(IPAddr("10.0.0.1")), Host(IPAddr("10.0.0.2")), 1)
		# h2 (c2) -> h1 (c1)
		OpenFlow.create_path_4ever(connection, Host(IPAddr("10.0.0.2")), Host(IPAddr("10.0.0.1")), 2)

def launch(deltau=None, poll=None, path=None):
	global POLL_PERIOD, PATH, DELTAU
	if deltau is not None:
		DELTAU = float(deltau)
	if poll is not None:
		POLL_PERIOD = float(poll)
	if path is not None:
		PATH = str(path)
	print "INFO deltaU = ", DELTAU
	print "INFO poll = ", POLL_PERIOD
	core.registerNew(ControlApp)
