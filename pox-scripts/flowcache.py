# Copyright (c) 2015 Mohamed Aslan <maslan@sce.carleton.ca>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

import time

class FlowCache(object):
	def __init__(self):
		self.cache = dict()	
	def add(self, key, ttl, to_ip, to_mac, to_port):
		expiry = float(time.time() + ttl)
		self.cache[key] = (expiry, to_ip, to_mac, to_port)
	def has(self, key):
		if key not in self.cache:
			return False
		if time.time() < self.cache[key][0]:
			return True
		return False
	def getToIP(self, key):
		if self.has(key) == False:
			return None
		return self.cache[key][1]
	def getToMAC(self, key):
		if self.has(key) == False:
			return None
		return self.cache[key][2]
	def getToPort(self, key):
		if self.has(key) == False:
			return None
		return self.cache[key][3]

