# Copyright (c) 2015 Mohamed Aslan <maslan@sce.carleton.ca>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

from pox.core import core
import pox.openflow.libopenflow_01 as of
from pox.lib.addresses import IPAddr, EthAddr
#from pox.lib.packet.ipv4 import ipv4
from pox.lib.packet.arp import arp
from pox.lib.packet.ethernet import ethernet

from hosts import Host, OpenFlow

class VirtualServer(object):
	def __init__(self, ip_addr, mac_addr):
		self.ip_addr = ip_addr
		self.mac_addr = mac_addr

	def send_arp_reply(self, connection, port, arp_req):
		print "DEBUG: making arp reply"
		rep = arp()
		rep.opcode = arp.REPLY
		rep.hwsrc = self.mac_addr
		rep.hwdst = arp_req.hwsrc
		rep.protosrc = self.ip_addr
		rep.protodst = arp_req.protosrc

		print "DEBUG: making ether packet"
		ether = ethernet()
		ether.type = ethernet.ARP_TYPE
		ether.src = self.mac_addr
		ether.dst = arp_req.hwsrc
		ether.set_payload(rep)

		print "DEBUG: sending arp reply"
		msg = of.ofp_packet_out()
		msg.data = ether.pack()
		msg.actions.append(of.ofp_action_output(port = of.OFPP_IN_PORT))
		msg.in_port = port
		connection.send(msg)

