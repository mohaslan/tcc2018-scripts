# Copyright (c) 2015 Mohamed Aslan <maslan@sce.carleton.ca>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

from __future__ import division
from pox.core import core
import pox.openflow.libopenflow_01 as of
from pox.lib.util import dpid_to_str
from pox.lib.util import str_to_bool
from pox.lib.packet.ipv4 import ipv4
from pox.lib.packet.arp import arp
import time
import thread
import json
import sys
from pox.lib.recoco import Timer
from pox.openflow.of_json import *
from threading import Lock

from virtserver import VirtualServer
from hosts import Server, Host, OpenFlow
from flowcache import FlowCache

import os.path

log = core.getLogger()

VIRT_SERVER_IP = IPAddr("10.0.0.254")
VIRT_SERVER_MAC = EthAddr("00:00:00:00:00:EF")

conn_list = dict()

POLL_PERIOD = 2

copy_util_server1 = 0
copy_util_server2 = 0

PATH = "."	# log files path

class LoadBalancer(object):
	def __init__ (self, connection, myID):
		self.connection = connection

		self.myID = myID
		if myID == 1:
			self.servers = [Server("10.0.0.3", "00:00:00:00:00:03", 3, '00-00-00-00-00-01'), Server("10.0.0.4", "00:00:00:00:00:04", 1, '00-00-00-00-00-02')]
		elif myID == 2:
			self.servers = [Server("10.0.0.4", "00:00:00:00:00:04", 3, '00-00-00-00-00-02'), Server("10.0.0.3", "00:00:00:00:00:03", 1, '00-00-00-00-00-01')]

		self.servers[0].set_log_path(PATH)
		self.servers[1].set_log_path(PATH)

		self.tx_packets = 0
		self.rx_packets = 0
		self.tx_server = 0
		self.rx_server = 0
		self.tx_dropped = 0
		self.rx_dropped = 0
		self.last_server = 0

		self.all_tx_pkts = 0
		self.all_tx_bytes = 0
		self.diff_tx_pkts = 0
		self.diff_tx_bytes = 0
		self.all_rx_pkts = 0
		self.all_rx_bytes = 0
		self.diff_rx_pkts = 0
		self.diff_rx_bytes = 0

		self.macToPort = {}

		# virtual server
		self.vserver = VirtualServer(VIRT_SERVER_IP, VIRT_SERVER_MAC)

		connection.addListeners(self)

		self.can_log = False
		self.cache = FlowCache()
		Timer(POLL_PERIOD, self.poll_func, recurring=True)
		# sync clock with other modules
		thread.start_new_thread(self.clock, ())

	def clock(self):
		while not os.path.exists("/tmp/sync.lock"):
			time.sleep(0.5)
		self.can_log = True
		self.start_time = time.time()

	def next_server(self):
		global copy_util_server1, copy_util_server2
		if self.myID == 1:
			other_util = copy_util_server2
		elif self.myID == 2:
			other_util = copy_util_server1

		if self.servers[0].get_utilization() <= other_util:
			server = 0
		else:
			server = 1
		print "server: ", server
		return self.servers[server]
		#return self.servers[0]

	def poll_func(self):
		if self.can_log == True:
			now = time.time() - self.start_time
			with open(PATH + "/switch-port3-" + str(self.myID) + ".csv", "a") as f:
				f.write("%.2f, %d, %d, %d\n" % (now, self.servers[0].packets, self.servers[0].utilization, self.servers[0].drops))
			with open(PATH + "/switch-port-all-" + str(self.myID) + ".csv", "a") as ff:
				ff.write("%.2f, %d, %d, %d, %d\n" % (now, self.diff_tx_pkts, self.diff_tx_bytes, self.diff_rx_pkts, self.diff_rx_bytes))
		self.connection.send(of.ofp_stats_request(body=of.ofp_port_stats_request()))

	def _handle_PortStatsReceived(self, event):
		global copy_util_server1, copy_util_server2

		tot_rx = tot_rx_by = 0
		tot_tx = tot_tx_by = 0
		for p in event.stats:
			if 4 <= p.port_no <= 35:
				tot_rx += p.rx_packets
				tot_rx_by += p.rx_bytes
			elif p.port_no == 3:
				tot_tx += p.tx_packets
				tot_tx_by += p.tx_bytes

		self.diff_tx_pkts = tot_tx - self.all_tx_pkts
		self.diff_tx_bytes = tot_tx_by - self.all_tx_bytes
		self.diff_rx_pkts = tot_rx - self.all_rx_pkts
		self.diff_rx_bytes = tot_rx_by - self.all_rx_bytes

		self.all_tx_pkts = tot_tx
		self.all_tx_bytes = tot_tx_by
		self.all_rx_pkts = tot_rx
		self.all_rx_bytes = tot_rx_by

		for item in event.stats:
			if  item.port_no == 3:
				tx_pkts_diff = item.tx_packets - self.tx_packets
				rx_pkts_diff = item.rx_packets - self.rx_packets
				self.tx_packets = item.tx_packets
				self.rx_packets = item.rx_packets
				self.servers[0].packets = tx_pkts_diff

				tx_diff = item.tx_bytes - self.tx_server
				rx_diff = item.rx_bytes - self.rx_server
				self.tx_server = item.tx_bytes
				self.rx_server = item.rx_bytes
				load = tx_diff
				self.servers[0].set_utilization(load)

				tx_dropped_diff = item.tx_dropped - self.tx_dropped
				rx_dropped_diff = item.rx_dropped - self.rx_dropped
				self.tx_dropped = item.tx_dropped
				self.rx_dropped = item.rx_dropped
				self.servers[0].drops = tx_dropped_diff

				if self.myID == 1:
					copy_util_server1 = load
				elif self.myID == 2:
					copy_util_server2 = load

			"""
			dp1 += item.tx_dropped
			dp2 += item.rx_dropped
			with open(PATH + "/drops-all-" + str(self.myID) + ".csv", "a") as ff:
				ff.write("%d, %d\n" % (dp1, dp2))
			"""

	def _handle_FlowRemoved(self, event):
		#now = time.time() - self.start_time
		if event.hardTimeout:
			if event.ofp.match.dl_type == ethernet.IP_TYPE:
				#if event.ofp.match.nw_proto == ipv4.TCP_PROTOCOL or event.ofp.match.nw_proto == ipv4.UDP_PROTOCOL:
					self.servers[0].remove_flow((event.ofp.match.nw_src.toStr(), event.ofp.match.nw_dst.toStr()))
					#with open("removal1.csv", "a") as f:
					#	f.write("%d, %s, %s, %d %d\n" % (now, event.ofp.match.nw_src, event.ofp.match.nw_dst, servers[0].n_flows, servers[1].n_flows))

	def _handle_PacketIn(self, event):
		packet = event.parse()

		def drop():
			print "dropping"
			msg = of.ofp_flow_mod()
			msg.match = of.ofp_match.from_packet(packet)
			msg.idle_timeout = 10
			msg.hard_timeout = 30
			msg.buffer_id = event.ofp.buffer_id
			self.connection.send(msg)

		if packet.type == ethernet.IPV6_TYPE:
			return
			print "IPv6"
			# drop()

		elif packet.type == ethernet.IP_TYPE:
			ip_packet = packet.payload
			key = (ip_packet.srcip.toStr(), ip_packet.dstip.toStr())

			if self.cache.has(key) == True:
				to_ip = self.cache.getToIP(key)
				to_mac = self.cache.getToMAC(key)
				to_port = self.cache.getToPort(key)

				if ip_packet.dstip == self.servers[0].ip_addr:
					OpenFlow.replay_packet(self.connection, packet, self.servers[0].port, buf_id = event.ofp.buffer_id, data = event.ofp)
				else:
					OpenFlow.replay_packet_to(self.connection, packet, to_port, Host(to_ip, to_mac), buf_id = event.ofp.buffer_id, data = event.ofp)
				return

			if ip_packet.dstip == VIRT_SERVER_IP:
				server = self.next_server()

				if server.ip_addr == self.servers[0].ip_addr:
					self.servers[0].insert_flow(key)
					OpenFlow.create_bi_forward_path(self.connection, Host(ip_packet.srcip), Host(self.vserver.ip_addr, self.vserver.mac_addr), Host(server.ip_addr, server.mac_addr), server.port, event.port, feedback=True, bid=event.ofp.buffer_id, data=event.ofp)
				else:
					OpenFlow.create_bi_forward_path(self.connection, Host(ip_packet.srcip), Host(self.vserver.ip_addr, self.vserver.mac_addr), Host(server.ip_addr, server.mac_addr), server.port, event.port, bid=event.ofp.buffer_id, data=event.ofp)
				self.cache.add(key, 1.5, server.ip_addr, server.mac_addr, server.port)

			elif ip_packet.dstip == self.servers[0].ip_addr:
				self.servers[0].insert_flow(key)
				OpenFlow.create_path(self.connection, Host(ip_packet.srcip), Host(self.servers[0].ip_addr), self.servers[0].port, feedback=True, bid=event.ofp.buffer_id)
				OpenFlow.create_path(self.connection, Host(self.servers[0].ip_addr), Host(ip_packet.srcip), 1)
				self.cache.add(key, 1.5, self.servers[0].ip_addr, self.servers[0].mac_addr, self.servers[0].port)

			else:
				print "XXX packet to: ", ip_packet.dstip

		elif packet.type == ethernet.ARP_TYPE:
			#arp_packet = packet.payload
			arp_packet = packet.next
			print "ARP packet received"
			if arp_packet.opcode == arp.REQUEST:
				print "who has ", arp_packet.protodst, " tell ", arp_packet.protosrc
				if arp_packet.protodst == VIRT_SERVER_IP:
					self.vserver.send_arp_reply(self.connection, event.port, arp_packet)
				else:
					print "Else!"
					return

		else:
			print packet.src, " -> ", packet.dst, " : ", event.port, " -- ", self.connection
			print 'type: ', packet.type


class ControlApp(object):
	def __init__(self):
		global conn_list
		core.openflow.addListeners(self)

	def _handle_ConnectionDown(self, event):
		global conn_list
		dpid = dpid_to_str(event.connection.dpid)
		if dpid in conn_list:
			del conn_list[dpid]
		print "DOWN: ", dpid

	def _handle_ConnectionUp(self, event):
		global conn_list
		print "DPID: ", dpid_to_str(event.connection.dpid)
		dpid = dpid_to_str(event.connection.dpid)
		if dpid in conn_list:
			print "Switch already connected!"
		conn_list[dpid] = event.connection
		if dpid == '00-00-00-00-00-01':
			print "Switch #1 connected."
			# start the load-balancer
			LoadBalancer(event.connection, 1)
		elif dpid == '00-00-00-00-00-02':
			print "Switch #2 connected."
			# start the load-balancer
			LoadBalancer(event.connection, 2)

def launch(poll=None, path=None):
	global POLL_PERIOD, PATH
	if poll is not None:
		POLL_PERIOD = float(poll)
	if path is not None:
		PATH = str(path)
	print "INFO poll = ", POLL_PERIOD
	core.registerNew(ControlApp)
