#!/usr/bin/env python3
#
# Copyright (c) 2015 Mohamed Aslan <maslan@sce.carleton.ca>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

import math
from queue import Queue
import sys
import argparse

from random import uniform

CLK = 0
TTL = 2
CALC = 3

class Server(object):
	def __init__(self):
		self.pkts = list()
		self.stats = 0
		self.stats_begin = 0
		self.calc = 0
		self.calc_begin = 0
	def update(self):
		n_pkts = 0
		for p in self.pkts:
			if p >= self.stats_begin and p < CLK:
				n_pkts += 1
		self.stats = n_pkts
		self.stats_begin = CLK
	def calculate(self):
		n_pkts = 0
		for p in self.pkts:
			if p >= self.calc_begin and p < CLK:
				n_pkts += 1
		self.calc = n_pkts
		self.calc_begin = CLK

server = [Server(), Server()]

class Controller(object):
	def __init__(self, num, srv1, srv2):
		self.num = num
		self.srv1 = srv1
		self.srv2 = srv2
		self.L1 = 0
		self.L2 = 0

	def control(self, pkts):
		d = self.L1 - self.L2
		d2 = 0
		if d <= 0:
			for p in pkts:
				server[self.srv1].pkts.append(CLK + float(p))
			d2 = 1
		else:
			for p in pkts:
				server[self.srv2].pkts.append(CLK + float(p))
			d2 = 2


def main():
	global CLK

	# parse command line arguments
	arg_parser = argparse.ArgumentParser()
	arg_parser.add_argument('-f', '--file', help='flow input file', required=True)
	arg_parser.add_argument('-s', '--sync', help='sync period', required=True, type=float)
	arg_parser.add_argument('-p', '--poll', help='polling period', required=True, type=float)
	args = arg_parser.parse_args()

	# init
	c1 = Controller(1, 0, 1)
	c2 = Controller(2, 1, 0)
	calc_time = CALC
	sync_time = args.sync
	poll_time = args.poll
	tot = 0
	avg = 0
	cnt = 0

	with open(args.file, 'r') as f:
		# assume: <index>,<arrival>,<group>,<number>,<packets-list> 
		#            0        1        2       3          4
		for line in f:
			arr = line.split(",")
			pkts = arr[4].split(":")
			CLK = float(arr[1])
			if CLK >= calc_time + uniform(0, 0):
				server[0].calculate()
				server[1].calculate()
				delta = abs(server[0].calc - server[1].calc)
				sigma = server[0].calc + server[1].calc
				if sigma != 0:
					xi_b = delta / sigma
					avg += xi_b
					cnt += 1
					print("[" + str(CLK) + "] xi_b = " + str(delta) + " / "+ str(sigma) + " = " + str(xi_b))
				tot += sigma
				calc_time += CALC
			if int(arr[2]) == 0:
				c1.control(pkts)
			elif int(arr[2]) == 1:
				c2.control(pkts)
			if CLK >= poll_time + uniform(0, 0):
				server[0].update()
				server[1].update()
				c1.L1 = server[0].stats
				c2.L1 = server[1].stats
				poll_time += args.poll
			if CLK >= sync_time + uniform(0, 0):
				c1.L2 = c2.L1
				c2.L2 = c1.L1
				sync_time += args.sync
		f.close()
	if cnt != 0:
		print("XiB " + str(avg / cnt))
	else:
		print("XiB nan")
if __name__ == '__main__':
	main()

