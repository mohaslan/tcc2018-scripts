#!/usr/bin/env python3
#
# Copyright (c) 2015 Mohamed Aslan <maslan@sce.carleton.ca>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

import math
from queue import Queue
import sys
import argparse

CLK = 0
TTL = 2
CALC = 6

class Server(object):
	def __init__(self):
		self.flows = list()
		self.load = 0

server = [Server(), Server()]

class Controller(object):
	def __init__(self, srv1, srv2):
		self.srv1 = srv1
		self.srv2 = srv2

	def control(self):
		global CLK
		# D
		for t in server[self.srv1].flows:
			if CLK >= t + TTL:
				server[self.srv1].flows.remove(t)
		for t in server[self.srv2].flows:
			if CLK >= t + TTL:
				server[self.srv2].flows.remove(t)
		# L
		L1 = len(server[self.srv1].flows)
		L2 = len(server[self.srv2].flows)
		# d
		d = L1 - L2
		d2 = 0
		if d <= 0:
			server[self.srv1].flows.append(CLK)
			server[self.srv1].load += 1
			d2 = 1
		else:
			server[self.srv2].flows.append(CLK)
			server[self.srv2].load += 1
			d2 = 2

		#print("[" + str(CLK) + "] DECISION = " + str(d2) + ", L1 = " + str(L1) + ", L2 = " + str(self.L2))



def main():
	global CLK

	# parse command line arguments
	arg_parser = argparse.ArgumentParser()
	arg_parser.add_argument('-f', '--file', help='flow input file', required=True)
	args = arg_parser.parse_args()

	# init
	c = Controller(0, 1)
	calc_time = CALC
	tot = 0
	avg = 0
	cnt = 0

	with open(args.file, 'r') as f:
		# assume: <index>,<arrival>,<group>,<number>,<packets-list> 
		#            0        1        2       3          4
		for line in f:
			arr = line.split(",")
			#pkts = arr[4].split(":")
			CLK = float(arr[1])
			if CLK >= calc_time:
				delta = abs(server[0].load - server[1].load)
				sigma = server[0].load + server[1].load
				if sigma != 0:
					xi_f = delta / sigma
					avg += xi_f
					cnt += 1
					print("[" + str(CLK) + "] xi_f = " + str(delta) + " / "+ str(sigma) + " = " + str(xi_f))
				server[0].load = 0
				server[1].load = 0
				calc_time += CALC
				tot += sigma
			if int(arr[2]) == 0 or int(arr[2]) == 1:
				c.control()
		f.close()
	print("REMAIN 1: " + str(server[0].load) + ", 2: " + str(server[1].load))
	print("TOTAL = " + str(tot + server[0].load + server[1].load))
	print("AVG " + str(avg / cnt))
if __name__ == '__main__':
	main()

