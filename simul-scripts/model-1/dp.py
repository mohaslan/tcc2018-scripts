#!/usr/bin/env python3
#
# Copyright (c) 2015 Mohamed Aslan <maslan@sce.carleton.ca>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

import math
from queue import Queue
import sys
import argparse

from random import uniform

CLK = 0
TTL = 2
CALC = 3

class Server(object):
	def __init__(self):
		self.flows = list()
		self.load = 0
		self.load_pkts = 0

server = [Server(), Server()]

class Controller(object):
	def __init__(self, num, srv1, srv2):
		self.num = num
		self.srv1 = srv1
		self.srv2 = srv2
		self.L2 = 0

	def control(self, pkts):
		global CLK
		# D
		for t in server[self.srv1].flows:
			if CLK >= t + TTL + uniform(0, 0):
				server[self.srv1].flows.remove(t)
		# L1
		L1 = len(server[self.srv1].flows)
		# d
		d = L1 - self.L2
		d2 = 0
		if d <= 0:
			server[self.srv1].flows.append(CLK)
			server[self.srv1].load += 1
			server[self.srv1].load_pkts += len(pkts)
			d2 = 1
		else:
			server[self.srv2].flows.append(CLK)
			server[self.srv2].load += 1
			server[self.srv2].load_pkts += len(pkts)
			d2 = 2


def main():
	global CLK

	# parse command line arguments
	arg_parser = argparse.ArgumentParser()
	arg_parser.add_argument('-f', '--file', help='flow input file', required=True)
	arg_parser.add_argument('-s', '--sync', help='sync period', required=True, type=float)
	args = arg_parser.parse_args()

	# init
	c1 = Controller(1, 0, 1)
	c2 = Controller(2, 1, 0)
	calc_time = CALC
	sync_time = args.sync
	tot = 0
	avg = 0
	cnt = 0
	avg_b = 0
	cnt_b = 0

	with open(args.file, 'r') as f:
		# assume: <index>,<arrival>,<group>,<number>,<packets-list> 
		#            0        1        2       3          4
		for line in f:
			arr = line.split(",")
			pkts = arr[4].split(":")
			CLK = float(arr[1])
			if CLK >= calc_time + uniform(0, 0):
				# Xi F
				delta = abs(server[0].load - server[1].load)
				sigma = server[0].load + server[1].load
				if sigma != 0:
					xi_f = delta / sigma
					avg += xi_f
					cnt += 1
					print("[" + str(CLK) + "] xi_f = " + str(delta) + " / "+ str(sigma) + " = " + str(xi_f))
				# Xi B
				delta_b = abs(server[0].load_pkts - server[1].load_pkts)
				sigma_b = server[0].load_pkts + server[1].load_pkts
				if sigma_b != 0:
					xi_b = delta_b / sigma_b
					avg_b += xi_b
					cnt_b += 1
					print("[" + str(CLK) + "] xi_b = " + str(delta_b) + " / "+ str(sigma_b) + " = " + str(xi_b))
				# reset counters
				server[0].load = 0
				server[0].load_pkts = 0
				server[1].load = 0
				server[1].load_pkts = 0
				tot += sigma
				calc_time += CALC
			if int(arr[2]) == 0:
				c1.control(pkts)
			elif int(arr[2]) == 1:
				c2.control(pkts)
			if CLK >= sync_time + uniform(0, 0):
				c1.L2 = len(server[1].flows)
				c2.L2 = len(server[0].flows)
				sync_time += args.sync
		f.close()
	print("REMAIN 1: " + str(server[0].load) + ", 2: " + str(server[1].load))
	print("TOTAL = " + str(tot + server[0].load + server[1].load))
	print("XiF " + str(avg / cnt))
	print("XiB " + str(avg_b / cnt_b))
if __name__ == '__main__':
	main()

