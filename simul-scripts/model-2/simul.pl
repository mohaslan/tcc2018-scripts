#!/usr/bin/perl
#
# Copyright (c) 2016 Mohamed Aslan <maslan@sce.carleton.ca>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

use warnings;
use strict;
use Getopt::Std;

my %opts = ();
getopts("x:f:n:s:p:", \%opts);
my $exp = $opts{x} if defined $opts{x} or die ("usage -x");
my $infile = $opts{f} if defined $opts{f} or die ("usage -f");
my $n = $opts{n} if defined $opts{n} or die ("usage -n");
my $sync = $opts{s} if defined $opts{s} or die ("usage -s");
my $poll = $opts{p} if defined $opts{p} or die ("usage -p");

my $clk = 0;
my $ttl = 2;
my $next_sync = $sync;
my $next_poll = $poll;
my $snap = 2;
my $next_snap = $snap;
my @ploads = (0) x $n;			# pristine loads
my @loads = map [(0) x $n], 0 .. $n - 1;
my @flows;
my @ppkts = (0) x $n;			# pristine packet loads
#my @sample_ppkts = (0) x $n;		# pristine packet loads
my @prev_ppkts = (0) x $n;		# pristine packet loads in the previous period
my @pkts = map [(0) x $n], 0 .. $n - 1;
my $avgstd = 0;
my $avgstd_n = 0;
my $bytes_per_pkt = 4096;


sub dump_loads {
	for (my $ctl = 0 ; $ctl < $n ; $ctl++) {
		for (my $srv = 0; $srv < $n; $srv++) {
			print $loads[$ctl][$srv] . " ";
		}
		print "\n";
	}
}

sub find_lls {
	my $ctl = $_[0];
	my @arr;
	if ($exp eq "DP") {
		@arr = @loads;
	}
	elsif ($exp eq "DA") {
		@arr = @pkts;
	}
	my $least_idx  = 0;
	my $least_load = $loads[$ctl][0];
	for (my $srv = 1 ; $srv < $n ; $srv++) {
		if ($arr[$ctl][$srv] < $least_load) {
			$least_load = $arr[$ctl][$srv];
			$least_idx = $srv;
		}
	}
	return $least_idx;
}

sub trim_flows {
	foreach my $flow (@flows) {
		if ($clk >= $flow->{rem}) {
			$ploads[$flow->{dom}]--;
		}
	}
	@flows = grep { $_->{rem} > $clk } @flows;
}

sub sync_all {
	for (my $ctl = 0 ; $ctl < $n ; $ctl++) {
		for (my $srv = 0 ; $srv < $n ; $srv++) {
			next if ($ctl == $srv);
			$loads[$ctl][$srv] = $loads[$srv][$srv];
			$pkts[$ctl][$srv] = $pkts[$srv][$srv];
		}
	}
}

sub poll_all {
	for (my $dom = 0 ; $dom < $n ; $dom++) {
		$loads[$dom][$dom] = $ploads[$dom];
		$pkts[$dom][$dom] = $ppkts[$dom] - $prev_ppkts[$dom];
	}
}

sub snapshot {
	my $avg = 0;
	for (my $i = 0 ; $i < $n ; $i++) {
		#$avg += $ploads[$i];
		$avg += $ppkts[$i] * $bytes_per_pkt;
	}
	$avg /= $n;

	my $std = 0;
	for (my $i = 0 ; $i < $n ; $i++) {
		#$std += ($ploads[$i] - $avg) ** 2;
		$std += ($ppkts[$i] * $bytes_per_pkt - $avg) ** 2;
	}
	$std = sqrt($std / $n);

	$avgstd += $std;
	$avgstd_n++;
}

open my $ifp, $infile or die 'error: could not open input file';
my @lines = <$ifp>;
foreach (@lines) {
	# assume: <index>,<arrival>,<group>,<number>,<packets-list>
	# assume: sorted in ascending order by inter-arrival time
	my @param = split /\,/, $_;
	my $atime = $param[1];			# inter-arrival time
	my $adom = $param[2];			# arrival domain
	my @ptime = split /\:/, $param[4];	# packets inter-arrival times

	# advance clock
	$clk = $atime;				# based on second assumption

	trim_flows;				# remove expired flows
	if ($clk >= $next_poll) {
		@prev_ppkts = @ppkts;
	}

	#dump_loads;
	my $dom = find_lls($adom);		# execute control
	#print "decision is $dom by C$adom\n";
	$ploads[$dom]++;
	push(@flows, {rem => $atime + $ttl, dom => $dom});
	$ppkts[$dom] += scalar(@ptime);

	# poll
	if ($clk >= $next_poll) {
		poll_all;
		#print "poll at $clk\n";
		$next_poll += $poll;
	}

	# sync
	if ($clk >= $next_sync) {
		sync_all;
		#print "sync at $clk\n";
		$next_sync += $sync;
	}

	# snapshot
	if ($clk >= $next_snap) {
		snapshot;
		$next_snap += $snap;
	}
}

$avgstd /= $avgstd_n if $avgstd_n;
print "$avgstd\n";
