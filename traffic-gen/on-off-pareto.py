#!/usr/bin/env python3
#
# Copyright (c) 2015 Mohamed Aslan <maslan@sce.carleton.ca>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#!/usr/bin/env python

import numpy as np
import sys
#import matplotlib.pyplot as plt
import argparse
import math

# parse command line arguments
arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('-l', '--time-length', type=float, help='set the time length of the experiment (sec)', required=True)
arg_parser.add_argument('-t', '--flow-ttl', type=float, help='set the flow time to live', required=True)
arg_parser.add_argument('-r', '--flow-rate', type=str, help='set the flow average arrival rate (flows/sec) at switches', required=True)
arg_parser.add_argument('-b', '--mean-on', type=str, help='set the mean burst time at switches', required=True)
arg_parser.add_argument('-i', '--mean-off', type=str, help='set the mean idle time at switches', required=True)
arg_parser.add_argument('-p', '--pkt-rate', type=str, help='set the packets rate per sec for flows', required=True)
arg_parser.add_argument('-o', '--out-file', type=str, help='set the output file name', required=True)
arg_parser.add_argument('-g', '--groups', type=int, help='set the number of groups', required=True)
arg_parser.add_argument('-n', '--nodes', type=int, help='set the number of nodes per group', required=True)
args = arg_parser.parse_args()


ttl = args.flow_ttl
length = args.time_length
csvfilename = args.out_file
n_grps = args.groups
n_nodes = args.nodes
prate = list(map(int, args.pkt_rate.split(",")))

# Poisson Flow
rate = list(map(int, args.flow_rate.split(",")))
_lambda = [float(1.0 / r) for r in rate]
n_rvs = [int(length * r) for r in rate]

# Pareto
shape = 1.5
mean_on = list(map(float, args.mean_on.split(",")))	# avg burst time
mean_off = list(map(float, args.mean_off.split(",")))	# avg idle time
# n pkts
nps = [int(mean_on[i] * prate[i]) for i in range(len(prate))]
scale_on = [float(np_ * ((shape - 1) / shape)) for np_ in nps]

#print "D1: ON shape = ", shape, ", scale = ", scale1_on
#print "D2: ON shape = ", shape, ", scale = ", scale2_on

scale_off = [float(mean * ((shape - 1) / shape)) for mean in mean_off]

#print "D1: OFF shape = ", shape, ", scale = ", scale1_off
#print "D2: OFF shape = ", shape, ", scale = ", scale2_off


"""
if size > 1, it will return an array of R.V.s
"""
def pareto_rv(scale, size=1):
	if size == 1:
		return ((np.random.pareto(shape, size) + 1) * scale)[0]
	else:
		return (np.random.pareto(shape, size) + 1) * scale

def gen_pkts(mean_on, scale_on, scale_off):
	plist = ''
	t = 0.0
	while t < 2.0:
		n_pkts = int(math.ceil(pareto_rv(scale_on)))
		if n_pkts == 0:
			n_pkts = 1	# at least 1 pkt
		tpp = mean_on / n_pkts
		# CBR
		for i in range(n_pkts):
			plist += ':' + str(t)
			t += tpp
			if t >= 2.0:
				break
		off_period = pareto_rv(scale_off)
		t += off_period
	return plist[1:]

def generate():
	# csv
	csv = []
	# nodes timestamps, used for validation
	ts = [None] * n_grps
	for i in range(0, n_grps):
		ts[i] = [0] * n_nodes

	arivals = [None] * n_grps
	for i in range(0, n_grps):
		arrivals = np.random.exponential(_lambda[i], n_rvs[i]).cumsum()

	i = 0
	for j in range(0, n_grps):
		for a in arrivals:
			i += 1
			# load-balance flows among nodes
			n = (i - 1) % n_nodes
			# flow validity check
			if a <= ts[j][n]:
				return None
			ts[j][n] = a + ttl
			plist = gen_pkts(mean_on[j], scale_on[j], scale_off[j])
			# <index>,<arrival time>,<group>,<number>,<pkt-list>
			csv.append(str(i) + ',' + str(float(a)) + ',' + str(j) + ',' + str(n) + ',' + plist)
	return csv

csv = None
trial = 1
while csv is None:
	print("trial %d." % trial)
	csv = generate()
	trial += 1

with open(csvfilename, 'w') as f:
	for line in csv:
		f.write("%s\n" % line)
	f.close()



#def empericalDistribution(X):
#	x = np.sort(X)
#	y = np.linspace(1./len(X), 1., num = len(X))
#	return x, y

#x, y = empericalDistribution(s)
#plt.plot(x, y)
#plt.savefig(gfxfile)
