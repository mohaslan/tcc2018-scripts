#!/usr/bin/env python3
#
# Copyright (c) 2015 Mohamed Aslan <maslan@sce.carleton.ca>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#!/usr/bin/env python

import numpy as np
import sys
#import matplotlib.pyplot as plt
import argparse

# parse command line arguments
arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('-r', '--flow-rate', type=str, help='set the flow average arrival rate (flows/sec)', required=True)
arg_parser.add_argument('-t', '--flow-ttl', type=float, help='set the flow time to live', required=True)
arg_parser.add_argument('-l', '--time-length', type=float, help='set the time length of the experiment (sec)', required=True)
arg_parser.add_argument('-p', '--pkts-per-flow', type=str, help='set the number of packets per flow', required=True)
arg_parser.add_argument('-o', '--out-file', type=str, help='set the output file name', required=True)
arg_parser.add_argument('-g', '--groups', type=int, help='set the number of groups', required=True)
arg_parser.add_argument('-n', '--nodes', type=int, help='set the number of nodes per group', required=True)
args = arg_parser.parse_args()

rate = list(map(int, args.flow_rate.split(",")))
ttl = args.flow_ttl
length = args.time_length
csvfilename = args.out_file
n_grps = args.groups
n_nodes = args.nodes
prate = list(map(int, args.pkts_per_flow.split(",")))

_lambda = [float(1.0 / r) for r in rate]
n_rvs = [int(length * r) for r in rate]

for i in range(0, n_grps):
	print("generating %d events with lambda = %f" % (n_rvs[i], _lambda[i]))

def gen_pkts(rate):
	n_rvs = int(ttl * rate)
	_lambda = float(1.0 / rate)
	s = np.random.exponential(_lambda, n_rvs)
	arrival = s.cumsum()
	plist = ''
	for a in arrival:
		plist += ':' + str(a)
	return plist[1:]

def generate():
	# csv
	csv = [''] * sum(n_rvs)
	# nodes timestamps, used for validation
	ts = [None] * n_grps
	for i in range(0, n_grps):
		ts[i] = [0] * n_nodes

	s = [None] * n_grps
	arrival = [None] * n_grps
	for i in range(0, n_grps):
		s[i] = np.random.exponential(_lambda[i], n_rvs[i])
		arrival[i] = s[i].cumsum()

	i = 0
	for j in range(0, n_grps):
		for a in arrival[j]:
			i += 1
			# load-balance flows among nodes
			n = (i - 1) % n_nodes
			# flow validity check
			if a <= ts[j][n]:
				return None
			ts[j][n] = a + ttl
			plist = gen_pkts(prate[j])
			# <index>,<arrival time>,<group>,<number>,<pkt-list>
			csv[i - 1] =  str(i) + ',' + str(a) + ',' + str(j) + ',' + str(n) + ',' + plist
	return csv

csv = None
trial = 1
while csv is None:
	print("trial %d." % trial)
	csv = generate()
	trial += 1

with open(csvfilename, 'w') as f:
	for line in csv:
		f.write("%s\n" % line)
	f.close()



#def empericalDistribution(X):
#	x = np.sort(X)
#	y = np.linspace(1./len(X), 1., num = len(X))
#	return x, y

#x, y = empericalDistribution(s)
#plt.plot(x, y)
#plt.savefig(gfxfile)
